1. Get the code by cloning it or getting a zip of it.
2. Unzip code if required
3. Get into the directory
4. Get Python
5. Get all the dependencies. Look at `requirements.txt` to find out more. (I use Anaconda, so beware!)
6. Now run the `main.py` file with python if you want to see how it looks. Otherwise run `server/app_server.py` file.
