from server import app_server as aps
import webbrowser
from threading import Thread

def start_server():
  server = Thread(target = aps.app_server)
  server.daemon = True
  server.start()
  # aps.app_server()

def start_webview():
  webbrowser.open_new_tab('http://localhost:8080/')

if __name__ == '__main__':
  print "::INFO: PyGet - The pure python File Downloader."
  print "::INFO: Starting server daemon"
  start_server()
  print "::INFO: Starting webview. Press any key to exit."
  start_webview()
  raw_input()
  print "::INFO: Exiting... Thanks for using PyGet!"
