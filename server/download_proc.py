# imports
import pycurl, uuid, Queue, os, time

# methods for Queues (multi-thread channels)
def get_safe(que):
  if not que: return None
  try:
    res = que.get_nowait()
  except Queue.Empty:
    res = None
  return res

def put_safe(que, res):
  if not que: return False
  try:
    que.put_nowait(res)
    res = True
  except Queue.Full:
    res = False
  return res

# a utility method for Curl object
def percent_util(c):
  return round((float(c.getinfo(pycurl.SIZE_DOWNLOAD))*100 / c.getinfo(pycurl.CONTENT_LENGTH_DOWNLOAD)) % 100, 2);

# Request queue
class RequestQueue(object):
  """
  This object represents the queue
  to hold all requests.
  """
  queue = []

  def __init__(self):
    super(RequestQueue, self).__init__()

  def addToQueue(self, uid, url, filename):
    self.queue.append((uid, url, filename))

  def removeFromQueue(self, uid):
    for tup in self.queue:
      if tup[0] == uid: queue.remove(tup)

  def getQueue(self):
    return self.queue

# PyCurl objects
class RequestMethods(object):
  """
  This object takes care of the actual
  downloading process. Maximum number
  of concurrent connections can be set
  during init (default = 10).
  """
  maxConcurrentConn = 10
  pathDownloads = 'downloads'

  m = pycurl.CurlMulti()
  m.handles = []
  m.active = []

  q = RequestQueue()

  qdts, qstd = None, None

  def __init__(self, maxConn=10, qstd=None, qdts=None, path_for_downloads='downloads'):
    super(RequestMethods, self).__init__()

    self.pathDownloads = path_for_downloads

    # Pre-allocate a list of curl objects
    self.maxConcurrentConn = maxConn
    for i in range(self.maxConcurrentConn):
      c = pycurl.Curl()
      c.fp = None
      c.setopt(pycurl.FOLLOWLOCATION, True)
      c.setopt(pycurl.MAXREDIRS, 5)
      c.setopt(pycurl.CONNECTTIMEOUT, 30)
      c.setopt(pycurl.TIMEOUT, 300)
      c.setopt(pycurl.NOSIGNAL, True)
      self.m.handles.append(c)

    # Set multi-thread channels
    self.qstd = qstd
    self.qdts = qdts

  def addToRequestQueue(self, addList):
    for req in addList:
      self.q.addToQueue(req[0], req[1], req[2])

  def mainLoop(self):
    # Copy handle list. This list
    # keeps track of free Curl
    # handles available for use
    freelist = self.m.handles
    activelist = self.m.active
    toGracefulExit = False
    lastGracefulSleep = time.time()
    flagResReset = True

    # infinite loop
    while True:
      # If there is a url to process and a free Curl object, add to multi stack
      queue = self.q.getQueue()
      while queue and freelist:
        uid, url, filename = queue.pop(0)
        full_filename = self.pathDownloads + os.sep + filename
        c = freelist.pop()
        c.fp = open(full_filename, "wb")
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.WRITEDATA, c.fp)
        self.m.add_handle(c)
        # store some info
        c.uid = uid
        c.filename = filename
        c.url = url
        activelist.append(c)

      # Run the internal curl state machine for the multi stack
      while True:
        ret, num_handles = self.m.perform()
        if ret != pycurl.E_CALL_MULTI_PERFORM: break

      if flagResReset:
        # our response object for putting out status of transfers
        res = []
        flagResReset = False

      # Check for curl objects which have terminated, and add them to the freelist
      while True:
        num_q, ok_list, err_list = self.m.info_read()
        for c in ok_list:
          c.fp.close()
          c.fp = None
          self.m.remove_handle(c)
          #sending success stuff
          res_obj = {
              'mode': 'success',
              'uid': c.uid,
              'url': c.url,
              'fileName': c.filename,
              'totalSize': c.getinfo(pycurl.CONTENT_LENGTH_DOWNLOAD)
            }
          res.append(res_obj)
          print "::LOG: Success:", c.uid, c.filename, c.url
          for cc in activelist:
            if cc.uid == c.uid: activelist.remove(cc)
          freelist.append(c)

        for c, errno, errmsg in err_list:
          c.fp.close()
          c.fp = None
          self.m.remove_handle(c)
          # sending failed stuff
          res_obj = {
              'mode': 'failed',
              'uid': c.uid,
              'url': c.url,
              'fileName': c.filename,
              'error': {
                  'errorNo': errno,
                  'errMsg': errmsg
                }
            }
          res.append(res_obj)
          print "::LOG: Failed:", c.uid, c.filename, c.url, errno, errmsg
          for cc in activelist:
            if cc.uid == c.uid: activelist.remove(cc)
          freelist.append(c)

        if num_q == 0:
          break

      # sending active stuff
      for c in activelist:
        res_obj = {
            'mode': 'active',
            'uid': c.uid,
            'url': c.url,
            'fileName': c.filename,
            'totalSize': c.getinfo(pycurl.CONTENT_LENGTH_DOWNLOAD),
            'doneSize': c.getinfo(pycurl.SIZE_DOWNLOAD),
            'avgSpeed': c.getinfo(pycurl.SPEED_DOWNLOAD),
            'percentDone': percent_util(c)
           }
        res.append(res_obj)

      # sending pending stuff
      for tup in queue:
        res_obj = {
            'mode': 'pending',
            'uid': tup[0],
            'url': tup[1],
            'fileName': tup[2]
          }
        res.append(res_obj)

      # send full status response
      if time.time() - lastGracefulSleep > 2.0:
        if put_safe(self.qdts,res):
          print "::LOG: Status of Transfers sent successfully to Queue."
          flagResReset = True
        else:
          print "::ERR: Status send to Queue failed."
        lastGracefulSleep = time.time()

      # Do other stuff here.
      # For example, check if any requests:
      req = get_safe(self.qstd)
      if req:
        for record in req:
          print record, type(record) # debug
          # 1. Need to be added to RequestQueue
          if record['mode'] == 'add':
            self.q.addToQueue(str(uuid.uuid4()), record['url'], record['fileName'])
            continue

          # 2. Need to be removed from RequestQueue
          if record['mode'] == 'remove':
            # if already has curl object assigned to download
            for c in activelist:
              if c.uid == record['uid']:
                c.fp.close()
                c.fp = None
                self.m.remove_handle(c)
                activelist.remove(c)
                freelist.append(c)
            # if hasn't started downloading
            self.q.removeFromQueue(record['uid'])
            continue

          # 3. Paused
          if record['mode'] == 'pause':
            for c in activelist:
              if c.uid == record['uid']:
                c.pause(pycurl.PAUSE_ALL)
            continue

          # 4. Resumed
          if record['mode'] == 'resume':
            for c in activelist:
              if c.uid == record['uid']:
                c.pause(pycurl.PAUSE_CONT)
            continue

          # 5. Exit
          if record['mode'] == 'exit':
            toGracefulExit = True

      # exit infinite loop
      if toGracefulExit:
        break

      # For now let's sleep till more data ia available
      # self.m.select(10.0)
      time.sleep(1)

  def cleanup(self):
    for c in self.m.handles:
      if c.fp is not None:
        c.fp.close()
        c.fp = None
      c.close()
    self.m.close()

# The main downloader daemon
def mainDaemon(maxConn, qstd, qdts, path_for_downloads):
  print "::INFO: Init downloader daemon..."
  prod = RequestMethods(maxConn, qstd, qdts, path_for_downloads)
  print "::INFO: Daemon init successfull. Starting main loop..."
  prod.mainLoop()
  print "::INFO: Main loop as ended. Init cleanup..."
  prod.cleanup()
  print "::INFO: Cleanup done. Safe to exit. Exiting daemon."

if __name__ == '__main__':

  # for testing only #
  mylist = [
    (uuid.uuid4(),'http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4', 'file1.mp4'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_2mb.mp4', 'file2.mp4'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/mp4/480/big_buck_bunny_480p_1mb.mp4', 'file3.mp4'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/mp4/480/big_buck_bunny_480p_2mb.mp4', 'file4.mp4'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/mp4/360/big_buck_bunny_360p_1mb.mp4', 'file5.mp4'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/mp4/360/big_buck_bunny_360p_2mb.mp4', 'file6.mp4'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/mp4/240/big_buck_bunny_240p_1mb.mp4', 'file7.mp4'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/mp4/240/big_buck_bunny_240p_2mb.mp4', 'file8.mp4'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/flv/720/big_buck_bunny_720p_1mb.flv', 'file9.flv'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/flv/720/big_buck_bunny_720p_2mb.flv', 'file10.flv'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/flv/720/big_buck_bunny_720p_5mb.flv', 'file11.flv'),
    (uuid.uuid4(),'http://www.sample-videos.com/video/flv/720/big_buck_bunny_720p_5mb.flv', 'file12.flv')
  ]

  test = RequestMethods(4)
  test.addToRequestQueue(mylist)
  test.mainLoop()
  test.cleanup()
