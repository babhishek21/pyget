# imports
import json, gevent, os, Queue
from threading import Thread, Event
from bottle import (
  Bottle,
  route,
  run,
  template,
  static_file,
  redirect,
  debug,
  request,
  abort
)

# globals
local_path = os.path.abspath(os.path.join(os.path.realpath(__file__), os.pardir, os.pardir))
path_for_static_assets = os.path.join(local_path, 'static')
path_for_static_assets += os.sep

path_for_downloads = os.path.join(local_path, 'downloads')
if not os.path.exists(path_for_downloads):
  os.mkdir(path_for_downloads)
# print path_for_static_assets, local_path # debug

# methods
def get_safe(que):
  try:
    res = que.get_nowait()
  except Queue.Empty:
    res = None
  return res

def put_safe(que, res):
  try:
    que.put_nowait(res)
    res = True
  except Queue.Full:
    res = False
  return res

def app_server(
    host = '0.0.0.0',
    port = 8080,
    onSocket = True,
    isReloader = False
  ):
  # make the Queues
  qstd = Queue.Queue() # Queue server to daemon
  qdts = Queue.Queue() # Queue daemon to server

  # start downloader file daemon
  import download_proc as dlp
  downloader = Thread(target=dlp.mainDaemon, args=(10, qstd, qdts, path_for_downloads))
  downloader.daemon = True
  downloader.start()

  # set up Bottle app server
  myapp = Bottle()

  @myapp.route('/')
  @myapp.route('/home')
  @myapp.route('/status')
  def home():
    redirect('/index.html')

  @myapp.route('/add')
  def addDl():
    redirect('/add_dl.html')

  @myapp.route('/done')
  @myapp.route('/misc')
  def misc():
    redirect('/done.html')

  @myapp.route('/<filename:path>')
  def send_static(filename):
    return static_file(filename, root=path_for_static_assets)

  @myapp.route('/websocket/get')
  def handle_get_ws():
    # client will ask servers for status
    wsock = request.environ.get('wsgi.websocket')
    if not wsock:
      abort(400, 'Expected WebSocket request.')

    while True:
      try:
        status_list = get_safe(qdts)
        if status_list:
          wsock.send(json.dumps(status_list))
          print "::LOG: /ws/get success. Status response sent." # debug
        gevent.sleep(0.5)
      except WebSocketError:
        print "::ERR: /ws/get failure." # debug
        break

  @myapp.route('/websocket/put')
  def handle_put_ws():
    wsock = request.environ.get('wsgi.websocket')
    if not wsock:
      abort(400, 'Expected WebSocket request.')

    while True:
      try:
        req = wsock.receive()
        # print req # debug
        print "::LOG: /ws/put request recieved. Trying to post..." # debug
        if req and put_safe(qstd, json.loads(req)):
          # print json.loads(req) # debug
          print "::LOG: /ws/put success. Request sent to daemon." # debug
          wsock.send("Request was recieved successfully. Passed along to daemon.")
        else:
          print "::ERR: /ws/put success. Request send to daemon failed." # debug
          wsock.send("Request was recieved successfully. But post to daemon failed.")
        gevent.sleep(0.5)
      except WebSocketError:
        print "::ERR: /ws/put failure." # debug
        break

  if onSocket:
    from gevent.pywsgi import WSGIServer
    from geventwebsocket import WebSocketError
    from geventwebsocket.handler import WebSocketHandler
    server = WSGIServer((host, port), myapp, handler_class=WebSocketHandler)
    server.serve_forever()
  else:
    myapp.run(host=host, port=str(port), onSocket=False, reloader=isReloader)

if __name__ == '__main__':
  # for testing only #
  app_server(isReloader = True)
