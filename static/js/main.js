var xferListData = {
  activeXfers: [],
  pendingXfers: [],
  doneXfers: []
}

function processResponse(response) {
  var result = JSON.parse(response);
  console.log(result);
  var active = xferListData.activeXfers;
  var pending = xferListData.pendingXfers;
  var done = xferListData.doneXfers;

  result.forEach(function(item) {
    switch(item.mode) {
      case "active":
      //check if in pending list
      for(var i=0; i<pending.length; i++) {
        if(pending[i].uid === item.uid){
          pending.splice(i, 1);
        }
      }
      // check if in active list
      for(var i=0; i<active.length; i++) {
        if(active[i].uid === item.uid){
          active.splice(i, 1);
        }
      }
      // add to active list
      active.push(item);
      break;

      case "pending":
      // check if already pending
      for(var i=0; i<pending.length; i++) {
        if(pending[i].uid === item.uid){
          pending.splice(i, 1);
        }
      }
      // add to pending
      pending.push(item);
      break;

      case "success":
      case "failed":
      // check in active
      for(var i=0; i<active.length; i++) {
        if(active[i].uid === item.uid){
          active.splice(i, 1);
        }
      }
      // add to done list
      done.push(item);
      break;
    }
  });

  xferListData.activeXfers = active;
  xferListData.pendingXfers = pending;
  xferListData.doneXfers = done;
}

function formatBytes(bytes,decimals) {
  if(bytes == 0) return '0 Byte';
  var k = 1000;
  var dm = decimals + 1 || 3;
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  var i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

Vue.filter('formatBytes', function(value) {
  return formatBytes(value, 2);
});

var xferList = new Vue({
  el: '#xferList',
  data: xferListData,
  methods: {
    resumeXfer: function(uid) {
      var obj = [{
        'mode': 'resume',
        'uid': uid
      }];
      myCustomSend(obj);
    },
    pauseXfer: function(uid) {
       var obj = [{
        'mode': 'pause',
        'uid': uid
      }];
      myCustomSend(obj);
    },
    removeXfer: function(uid) {
        var obj = [{
        'mode': 'remove',
        'uid': uid
      }];
      myCustomSend(obj);

      var active = xferListData.activeXfers;
      for(var i=0; i<active.length; i++) {
        if(active[i].uid === uid){
          active.splice(i, 1);
        }
      }
      xferListData.activeXfers = active;

      var pending = xferListData.pendingXfers;
      for(var i=0; i<pending.length; i++) {
        if(pending[i].uid === uid){
          pending.splice(i, 1);
        }
      }
      xferListData.pendingXfers = pending;
    },
    removeDoneXfer: function(uid) {
      // this one is different. data of transfers wich are done no longer available to server. so jsut need to remove from done list
      var done = xferListData.doneXfers;
      for(var i=0; i<done.length; i++) {
        if(done[i].uid === uid){
          done.splice(i, 1);
        }
      }
      xferListData.doneXfers = done;// ek bracket extra hai. woh hata de- line 137
    },
  },
  watch: {
    'activeXfers': {
      handler: function() {
        console.log('activeXfers updating...');
        $('.ui.indicating.progress').each(function() {
          $(this).progress({
            percent: $(this).attr('data-percent')
          });
        });
      },
      deep: true
    }
  }
});

$(document).ready(function() {
  $('.ui.indicating.progress').progress();
});

var this_host = location.host;
var sockURL = "ws://" + this_host + "/websocket/";


if ("WebSocket" in window) {
  // weboscket for get status
  var wsa = new WebSocket(sockURL + "get");
  wsa.onopen = function() {
    console.log('get opened')
  };
  wsa.onmessage = function (evt) {
    console.log('get request recieved a response');
    processResponse(evt.data);
  };
  wsa.onerror = function(err) {
    console.log(err);
  }
  wsa.onclose = function() {
    console.log('get closed')
  }

  // websocket for put requests
  var wsb = new WebSocket(sockURL + "put");
  wsb.onopen = function() {
    console.log('wsb is open');
  }
  wsb.onclose = function() {
    console.log('wsb is closed');
  }
  wsb.onmessage = function(event) {
    console.info(event.data);
  }

} else {
  console.error("Websockets not supported in this browser.");
}

function myCustomSend(data) {
  if(!wsb) {
    console.error("Websocket not available");
  } else {
    wsb.send(JSON.stringify(data));
  }
}
