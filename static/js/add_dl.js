var this_host = location.host;
var sockURL = "ws://" + this_host + "/websocket/";

if ("WebSocket" in window) {
  var addXferWs = new WebSocket(sockURL + "put");
  addXferWs.onopen = function() {
    console.log('addXferWs is open');
  }
  addXferWs.onclose = function() {
    console.log('addXferWs is closed');
  }
  addXferWs.onmessage = function(event) {
    console.info(event.data);
  }

} else {
  console.error("Websockets not supported in this browser.");
}

$('#addXferForm').submit(function(event) {
  event.preventDefault();

  // send form data over socket
  var formdata = $('#addXferForm').serializeArray().reduce(function(obj, item) {
    obj[item.name] = item.value;
    return obj;
  }, {});
  formdata['url'] = encodeURI(formdata['url']);
  formdata['fileName'] = formdata['fileName'].trim().replace(/\s+/g, '_');
  formdata['mode'] = "add";
  // console.log(formdata); // debug
  var reqdata = [formdata]

  if(!addXferWs) {
    console.error("Websocket not available.");
  } else {
    addXferWs.send(JSON.stringify(reqdata));
  }

  // go back to active xfers page
  // window.location.replace("/home");
});
